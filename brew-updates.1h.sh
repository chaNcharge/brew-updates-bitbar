#!/usr/bin/env zsh
# <bitbar.title>Homebrew Updates</bitbar.title>
# <bitbar.author>killercup</bitbar.author>
# <bitbar.author.github>killercup</bitbar.author.github>
# <bitbar.desc>List available updates from Homebrew (OS X)</bitbar.desc>

exit_with_error() {
  echo "err | color=red";
  exit 1;
}

sleep 10
nice -20 /usr/local/bin/brew update > /dev/null || exit_with_error;

PINNED=$(/usr/local/bin/brew list --pinned);
OUTDATED=$(/usr/local/bin/brew outdated --quiet);
CASKOUTDATED=$(/usr/local/bin/brew cask outdated --quiet);

UPDATES=$(comm -13 <(for X in "${PINNED[@]}"; do echo "${X}"; done) <(for X in "${OUTDATED[@]}"; do echo "${X}"; done))

BREW_UPDATE_COUNT=$(echo "$UPDATES" | grep -c '[^[:space:]]');
CASK_UPDATE_COUNT=$(echo "$CASKOUTDATED" | grep -c '[^[:space:]]');
UPDATE_COUNT=$(($CASK_UPDATE_COUNT+$BREW_UPDATE_COUNT));

echo "↑$UPDATE_COUNT | dropdown=false"
echo "---";
echo "Cleanup| bash=/usr/local/bin/brew param1=cleanup param2=-s terminal=false refresh=true"
if [ "$UPDATE_COUNT" -gt 0 ]; then
  echo "Upgrade all Formulae| bash=/usr/local/bin/brew param1=upgrade terminal=false refresh=true"
  echo "Upgrade all Casks | bash=/usr/local/bin/brew param1=cask param2=upgrade terminal=false refresh=true" 
  echo "Formulae:"
  echo "$UPDATES" | awk '{print $0 " | terminal=false refresh=true bash=/usr/local/bin/brew param1=upgrade param2=" $1}'
  echo "Casks:"
  echo "$CASKOUTDATED" | awk '{print $0 " | terminal=false refresh=true bash=/usr/local/bin/brew param1=cask param2=upgrade param3=" $1}'
fi
